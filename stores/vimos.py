import requests
from bs4 import BeautifulSoup
from unicodedata import normalize
from datetime import date
import pprint
import csv

HOST = 'https://viborg.vimos.ru/'
SEARCH_PAGE_URL = 'https://viborg.vimos.ru/new-search?text='
HEADERS = {
    'Accept': '*/*',
    'User-Agent': 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/113.0.0.0 Safari/537.36'
}

class vimos:

    @staticmethod
    def scrape_to_file(good_name):
        instance = vimos()
        instance.__csv = f'Вимос. {good_name}. {date.today()}.csv'
        search_page_html = vimos.__get_html(SEARCH_PAGE_URL+good_name.replace(" ", "+"))

        instance.__url = vimos.__get_category_url(search_page_html)
        instance.__parser()


    @staticmethod
    def __get_html(url, params=''):
        r = requests.get(url, headers=HEADERS, params=params)
        return r.text

    @staticmethod
    def __get_category_url(html):
        soup = BeautifulSoup(html, 'html.parser')
        # берем ссылки из тегов <a> в дереве категорий товаров,
        # кроме тех, которые относятся к страницам collections, потому что там не будет пагинации
        navigation_urls = [x.find('a')['href']
                           for x in soup.find_all('li', class_='digi-collections__breadcrumbs-item')
                           if 'collections' not in x.find('a')['href']]

        # возвращаем ссылку на последнюю категорию в дереве товаров
        # некоторые категории могут быть с неправильными ссылками - вместо рабочей ссылки
        'https://viborg.vimos.ru/catalog/doska-suhaa-stroganaa'
        # может быть нерабочая ссылка подобного вида
        'https://viborg.vimos.ru/catalog/doska-suhaya-stroganaya'

        return navigation_urls[-1].replace('ya', 'a')

    def __get_content(self, html):
        soup = BeautifulSoup(html, 'html.parser')
        items = soup.find_all('li', class_ = 'product-card')
        goods = []
        today_date = date.today()
        for item in items:
            the_title = item.select_one(".product-card__title")
            code = item.select_one(".product-card__code-data")
            price = item.select_one(".product-card__price")
            in_stock = item.select_one(".product-card__availability")
            link = item.find('div', class_='product-card__content').find('a').get('href')

            if all([the_title, code, price, in_stock, link, date]):
                goods.append(
                    {
                        'title': the_title.get_text().strip(),
                        'code': code.get_text().strip(),
                        'price': " ".join(normalize('NFKD', price.get_text()).split()),
                        'in_stock': in_stock.get_text().strip(),
                        'link': HOST+link,
                        'date': today_date
                }
                )
            else:
                print('cat says SHHHHH!')
        return goods

    def __save_doc(self, goods, path):
        with open (path, 'w', encoding='utf-8', newline='') as file:
            writer = csv.writer(file, delimiter=';')
            writer.writerow(['Название', 'Артикул', 'Цена', 'Наличие', 'Ссылка', 'Дата'])
            for item in goods:
                writer.writerow([item['title'], item['code'], item['price'], item['in_stock'], item['link'], item['date']])

    def __parser(self):
        PAGENATION = 1 # TODO: доделать пагинацию
        #PAGENATION = int(PAGENATION.strip())
        response = requests.get(self.__url, headers=HEADERS)
        if response.status_code == 200:
            goods = []
            for page in range(1, PAGENATION+1):
                page_url = f'{self.__url}/page/{page}?size=9'
                print(f'Парсим страницу {page}')
                html = self.__get_html(page_url)
                goods.extend(self.__get_content(html))
                self.__save_doc(goods, self.__csv)
            print(f'Парсинг закончился на {page} странице')
            #pprint.pprint(goods, sort_dicts=False)
        else:
            print("Error")

