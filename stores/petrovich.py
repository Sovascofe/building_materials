import requests
import pprint
import json
import csv
from datetime import date

HOST = 'https://vyborg.petrovich.ru/'
HEADERS = {
    'Accept': '*/*',
    'User-Agent': 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/113.0.0.0 Safari/537.36'
}

class petrovich:
    @staticmethod
    def scrape_to_file(good_name):
        self = petrovich()
        csv_name = f'Петрович. {good_name}. {date.today()}.csv'
        petrovich_ext_api = f'https://autocomplete.diginetica.net/autocomplete/?query=&shuffle=fa' \
                            f'lse&strategy=advanced,zero_queries&productsSize=20&regionId=vbg' \
                            f'&st={good_name.replace(" ", "+")}' \
                            f'&apiKey=170RDVB2N9&forIs=true'
        goods = self.__get_items(petrovich_ext_api)
        self.__save_doc(goods, csv_name)

    @staticmethod
    def __get_items(url):
        goods = []
        response = requests.get(url, headers=HEADERS)
        items = json.loads(response.text)['products']
        for item in items:
            goods.append(
                {
                    'title': item["name"],
                    'code': item["id"],
                    'price': f'{item["price"]} р',
                    'in_stock': item["available"],
                    'link': HOST + item["categories"][0]["link_url"] + item["id"],
                    'date': date.today().strftime('%d-%m-%Y')
                })

        return goods

    @staticmethod
    def __save_doc(goods, path):
        with open(path, 'w', encoding='utf-8', newline='') as file:
            writer = csv.writer(file, delimiter=';')
            writer.writerow(['Название', 'Артикул', 'Цена', 'Наличие', 'Ссылка', 'Дата'])
            for item in goods:
                writer.writerow(
                    [item['title'], item['code'], item['price'], item['in_stock'], item['link'], item['date']])