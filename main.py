from stores.vimos import vimos
from stores.petrovich import petrovich

def main():
    vimos.scrape_to_file(good_name="строганная доска")
    petrovich.scrape_to_file(good_name="строганная доска")

if __name__ == "__main__":
    main()